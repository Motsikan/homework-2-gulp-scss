let btn = document.querySelector(".header__burger-menu");

btn.addEventListener("click", () => {
  let list = document.querySelector(".menu__list");
  btn.classList.toggle("open");
  list.classList.toggle("--active");
});
