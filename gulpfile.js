import gulp from "gulp";
const { parallel, series, src, dest, watch } = gulp;

import browserSync from "browser-sync";
const bsServer = browserSync.create();

import dartSass from "sass";
import gulpSass from "gulp-sass";
const sass = gulpSass(dartSass);

import fileinclude from "gulp-file-include";

import clean from "gulp-clean";

import autoprefixer from "gulp-autoprefixer";

import gcmq from "gulp-group-css-media-queries"; 

import cleanCSS from "gulp-clean-css";

import rename from "gulp-rename";

import concat from "gulp-concat";

import minifyjs from "gulp-js-minify";

import imagemin from "gulp-imagemin";

const serve = () => {
  bsServer.init({
    server: {
      baseDir: "./",
      browser: "firefox.exe",
    },
  });
}

const cleaner = () => {
  return src("./dist/*", { read: false }).pipe(clean());
}

// const htmls = () =>  {
//   return src("./src/html/*.html")
//     .pipe(
//       fileinclude({
//         prefix: "@@",
//       })
//     )
//     .pipe(dest("./dist/"))
//     .pipe(bsServer.reload({ stream: true }));
// }

const images = () => {
  return src("./src/img/**/*.{jpg,jpeg,png,svg,webp}")
    .pipe(imagemin())
    .pipe(dest("./dist/img/"))
    .pipe(bsServer.reload({ stream: true }));
}

const styles = () => {
  return (
    src("./src/scss/style.scss")
      .pipe(sass().on("error", sass.logError))
      //  .pipe(gcmq())
      .pipe(
        autoprefixer(["last 15 versions", ">1%", "ie 8", "ie 7"], {
          cascade: true,
        })
      )
      .pipe(dest("./dist/css/"))
      .pipe(cleanCSS())
      .pipe(
        rename({
          extname: ".min.css",
        })
      )
      .pipe(dest("./dist/css/"))
      .pipe(bsServer.reload({ stream: true }))
  );
}

const scripts = () => {
  return (
    src("./src/js/**/*.js")
      .pipe(dest("./dist/js/"))
      .pipe(concat("index.min.js"))
      .pipe(minifyjs())
      //  .pipe(
      //    rename({
      //      extname: ".min.js",
      //    })
      //  )
      .pipe(dest("./dist/js/"))
      .pipe(bsServer.reload({ stream: true }))
  );
}

const watcher = () => {
  watch("./src/img/**/*.{jpg,jpeg,png,svg,webp}", images);
  watch("./src/scss/**/*.scss", styles);
  watch("./src/js/index.js", scripts);
  watch("./index.html").on("change", bsServer.reload);
}

gulp.task("build", gulp.series(cleaner, images, styles, scripts));
gulp.task("dev", gulp.series("build", gulp.parallel(serve, watcher)));


